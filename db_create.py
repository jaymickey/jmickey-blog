#!env/bin/python

from app import db
# Empty database must exist. Database either in path (printenv on Mac/Linux)
# or sqllite db will be created
db.create_all()

